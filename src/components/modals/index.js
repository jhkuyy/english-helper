import ActionModal from './ActionModal.vue';
import AddWordModal from './AddWordModal.vue';
import EditWordModal from './EditWordModal.vue';

export {
  ActionModal,
  AddWordModal,
  EditWordModal,
};
