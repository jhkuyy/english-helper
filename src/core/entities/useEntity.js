export const STORAGE_KEY = 'translations-db';

/**
 * @enum {string}
 */
export const EntityType = {
  TRANSITION: 'translation',
  DICTIONARY_WORD: 'dictionary-word',
};

export function useEntity(entityType) {
  const isEntity = (object) => object.$$entityType === entityType;

  const createEntity = (props = {}) => {
    Object.defineProperty(props, '$$entityType', {
      enumerable: false,
      configurable: false,
      writable: false,
      value: entityType,
    });

    return props;
  };

  return [createEntity, isEntity];
}

export function cloneEntity(entity) {
  if (!entity) {
    return null;
  }

  const [createEntity] = useEntity(entity.$$entityType);

  const cloned = JSON.parse(JSON.stringify(entity));

  return createEntity(cloned);
}

export const [createTranslation, isTranslation] = useEntity(EntityType.TRANSITION);
export const [createDictionaryWord, isDictionaryWord] = useEntity(EntityType.DICTIONARY_WORD);
