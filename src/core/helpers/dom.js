/**
 * @param el {Element}
 * @param className {string}
 * @param duration {number}
 */
export function addTemporaryClass(el, className, duration = 500) {
  el.classList.add(className);

  setTimeout(() => {
    el.classList.remove(className);
  }, duration);
}
