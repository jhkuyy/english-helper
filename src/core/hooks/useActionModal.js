import { ref } from 'vue';

let resolveShowModalPromise = null;
const actionResult = ref(null);
const activeActionModal = ref(null);

export function useActionModal() {
  const setActionResult = (action) => { actionResult.value = action; };

  /**
   * @param {string} options.title Title of the modal
   * @param {string} options.text Text of the modal
   * @param {array} options.actions  Available actions
   */
  const showActionModal = (options) => {
    activeActionModal.value = options;

    return new Promise((res) => {
      resolveShowModalPromise = res;
    });
  };

  const closeActionModal = () => {
    resolveShowModalPromise(actionResult.value);
    activeActionModal.value = null;
    setActionResult(null);
  };

  return {
    activeActionModal,
    setActionResult,
    showActionModal,
    closeActionModal,
  };
}
