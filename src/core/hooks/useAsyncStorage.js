// eslint-disable-next-line no-unused-vars
import { EntityType } from '@/core/entities/useEntity';
import { asyncStorage } from '@/core/storage';

import { useRequest } from './useRequest';

/**
 * @param type {EntityType}  a one of the enumeration values.
 */
export function useAsyncStorage(type) {
  const [add, addState] = useRequest((entity) => asyncStorage.add(entity));
  const [update, updateState] = useRequest((entity) => asyncStorage.update(entity));
  const [remove, removeState] = useRequest((entity) => asyncStorage.remove(entity));
  const [getById, getByIdState] = useRequest((id) => asyncStorage.getById(type, id));
  const [get, getState] = useRequest(() => asyncStorage.get(type));

  return {
    add: [add, addState],
    update: [update, updateState],
    remove: [remove, removeState],
    getById: [getById, getByIdState],
    get: [get, getState],
  };
}
