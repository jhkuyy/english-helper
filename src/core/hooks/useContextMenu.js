import { ref } from 'vue';

const activeContextMenu = ref(null);

export function useContextMenu() {
  /**
   * @param {Symbol} name Context menu name
   * @param {object} position Context menu position coordinates
   * @param {object} options Context menu props
   */
  const showContextMenu = (name, position, options) => {
    activeContextMenu.value = {
      name,
      position,
      options,
    };
  };

  const closeContextMenu = () => {
    activeContextMenu.value = null;
  };

  return {
    activeContextMenu,
    showContextMenu,
    closeContextMenu,
  };
}
