import { fetchTranslationList, fetchTranslationMap } from '@/core/http';

import { useRequest } from './useRequest';

export function useDictionaryApi() {
  const [fetchList, fetchListState, fetchListError] = useRequest((q) => fetchTranslationList(q));
  const [fetchMap, fetchMapState, fetchMapError] = useRequest((q) => fetchTranslationMap(q));

  return {
    fetchList: [fetchList, fetchListState, fetchListError],
    fetchMap: [fetchMap, fetchMapState, fetchMapError],
  };
}
