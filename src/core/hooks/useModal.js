import { defineAsyncComponent, ref } from 'vue';

const modalQueue = ref([]);

export const Modal = {
  ADD_WORD: 'ModalAddWord',
  EDIT_WORD: 'ModalEditWord',
};

const ModalMap = {
  [Modal.ADD_WORD]: () => import('@/components/modals/AddWordModal.vue'),
  [Modal.EDIT_WORD]: () => import('@/components/modals/EditWordModal.vue'),
};

export const components = Object.entries(ModalMap).reduce((map, [name, callback]) => ({
  ...map,
  [name]: defineAsyncComponent(callback),
}), {});

export function useModal() {
  const showModal = (name, options) => {
    modalQueue.value.unshift({
      name,
      options,
    });
  };

  const closeModal = () => {
    modalQueue.value.pop();
  };

  return {
    modalQueue,
    showModal,
    closeModal,
  };
}
