import { ref } from 'vue';

export const RequestState = {
  PENDING: Symbol('Request state pending'),
  SUCCESS: Symbol('Request state success'),
  ERROR: Symbol('Request state error'),
};

/*
 * TODO: Возвращать ответ, статус и ошибку в одном месте
 * Потому что сейчас использование этого хука выглядит как магия.
 * Не совсем понятно в какой момент меняется state.
 * Нужно пепеределать на:
 * const [response, state, error] = request();
 * Так будет логичнее и очевиднее.
 */
export function useRequest(func) {
  const state = ref(null);
  const error = ref(null);

  async function requestWrapper(...args) {
    state.value = RequestState.PENDING;
    try {
      const response = await func(...args);
      state.value = RequestState.SUCCESS;
      return response;
    } catch (e) {
      console.error('useRequest error', e);
      state.value = RequestState.ERROR;
      error.value = e.message;
    }

    return null;
  }

  return [requestWrapper, state, error];
}
