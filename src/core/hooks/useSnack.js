import { ref } from 'vue';

export const snackEventEnum = {
  SHOW: Symbol('SnackEvent:show'),
  HIDE: Symbol('SnackEvent:hide'),
};

const snacks = ref([]);
const showSnack = (text, options) => snacks.value.push({ text, options: { ...options, id: Date.now() } });
const hideSnack = (id) => snacks.value.splice(snacks.value.find((s) => s.options.id === id), 1);

const snack = {
  success: (text, options) => showSnack(text, { ...options, type: 'success' }),
  info: (text, options) => showSnack(text, { ...options, type: 'info' }),
  error: (text, options) => showSnack(text, { ...options, type: 'error' }),
};

export function useSnack() {
  return {
    snacks,
    snack,
    hideSnack,
  };
}
