import { onUnmounted } from 'vue';

import { useEventListener } from './useEventListener';

const eventList = ['mouseup'];

function getSelectedText() {
  const selection = window.getSelection();
  const text = selection.toString();

  if (!text) {
    return null;
  }

  const rect = selection.getRangeAt(0).getBoundingClientRect();

  return {
    text,
    rect,
  };
}

export function useTextSelection() {
  const onTextSelection = (target, callback) => {
    let disposables = eventList.map((event) => useEventListener(target, event, () => {
      const selectedText = getSelectedText();

      if (selectedText) {
        callback(selectedText);
      }
    }));

    const stopListener = () => {
      disposables.forEach((dispose) => dispose());
      disposables = [];
    };

    onUnmounted(stopListener);

    return stopListener;
  };

  return {
    onTextSelection,
  };
}
