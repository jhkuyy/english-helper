import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://translate.googleapis.com',
});

export async function fetchTranslations(q) {
  const {
    data: {
      dict = [],
      sentences = [],
    },
  } = await instance.get(`/translate_a/single?client=gtx&sl=en&tl=ru&hl=ru&dt=t&dt=bd&dj=1&source=icon&q=${q}`);

  return [dict, sentences];
}

export async function fetchTranslationList(q) {
  const [dict, sentences] = await fetchTranslations(q);

  const wordList = dict.reduce((result, item) => {
    const words = item.entry
      .filter(({ score }) => score > 0.005)
      .map(({ word }) => word);

    return [
      ...result,
      ...words,
    ];
  }, []);

  return [
    ...sentences.map(({ trans }) => trans),
    ...wordList,
  ];
}

export async function fetchTranslationMap(q) {
  const [response, sentences] = await fetchTranslations(q);

  const wordsMap = response.reduce((result, item) => {
    const words = item.entry
      .filter(({ score }) => score > 0.005)
      .map(({ word }) => word);

    if (words.length === 0) {
      return result;
    }

    return {
      ...result,
      [item.pos]: words,
    };
  }, {});

  return {
    sentences: sentences.map(({ trans }) => trans),
    ...wordsMap,
  };
}
