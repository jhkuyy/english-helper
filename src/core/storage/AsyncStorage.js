import { useEntity } from '@/core/entities/useEntity';
import { indexedDbProvider } from '@/core/storage/providers';

class AsyncStorage {
  #provider = null;

  constructor(provider) {
    this.#provider = provider;
  }

  static #transformResponse(type, response) {
    if (!response) {
      return null;
    }

    const [createEntity] = useEntity(type);

    if (Array.isArray(response)) {
      return response.map(createEntity);
    }

    return createEntity(response);
  }

  async init() {
    await this.#provider.init();
  }

  add(entity) {
    return this.#provider.add(entity.$$entityType, entity);
  }

  update(entity) {
    return this.#provider.update(entity.$$entityType, entity);
  }

  remove(entity) {
    return this.#provider.remove(entity.$$entityType, entity);
  }

  async getById(type, id) {
    const response = await this.#provider.getById(type, id);
    return AsyncStorage.#transformResponse(type, response);
  }

  async get(type) {
    const response = await this.#provider.get(type);
    return AsyncStorage.#transformResponse(type, response);
  }
}

export const asyncStorage = new AsyncStorage(indexedDbProvider);
