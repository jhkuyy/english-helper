import { EntityType, STORAGE_KEY } from '@/core/entities/useEntity';

const indexedDB = window.indexedDB ?? window.mozIndexedDB ?? window.webkitIndexedDB ?? window.msIndexedDB;
const DATABASE_VERSION = 2;

const transactionTypeEnum = {
  READ_ONLY: 'readonly',
  READ_WRITE: 'readwrite',
};

export class IndexedDbProvider {
  #database = null;

  #connection = null;

  constructor(database) {
    if (!indexedDB) {
      throw new Error('IndexedDB not supported');
    }

    this.#database = database;
  }

  init() {
    const connectionRequest = indexedDB.open(this.#database, DATABASE_VERSION);

    return new Promise((resolve, reject) => {
      connectionRequest.onerror = (e) => {
        reject(new Error(`Error connection to database. Code: ${e.target.errorCode}`));
      };

      connectionRequest.onupgradeneeded = (e) => {
        const db = e.target.result;

        Object.values(EntityType).forEach((entityType) => {
          if (!db.objectStoreNames.contains(entityType)) {
            db.createObjectStore(entityType, {
              keyPath: 'id',
              autoIncrement: true,
            });
          }
        });
      };

      connectionRequest.onsuccess = (e) => {
        this.#connection = e.target.result;
        resolve(true);
      };
    });
  }

  async #operation(type, transactionType, callback) {
    if (this.#connection === null) {
      await this.init();
    }

    const transaction = this.#connection.transaction([type], transactionType);

    const store = transaction.objectStore(type);

    const request = await callback(store);

    return new Promise((resolve, reject) => {
      request.onsuccess = (e) => {
        resolve(e.target.result);
      };
      request.onerror = () => {
        reject(new Error('Database error.'));
      };
    });
  }

  add(type, object) {
    return this.#operation(
      type,
      transactionTypeEnum.READ_WRITE,
      (store) => store.add(object),
    );
  }

  update(type, object) {
    return this.#operation(
      type,
      transactionTypeEnum.READ_WRITE,
      (store) => store.put(object),
    );
  }

  getById(type, id) {
    return this.#operation(
      type,
      transactionTypeEnum.READ_ONLY,
      (store) => store.get(id),
    );
  }

  get(type) {
    return this.#operation(
      type,
      transactionTypeEnum.READ_ONLY,
      (store) => store.getAll(),
    );
  }

  remove(type, entity) {
    return this.#operation(
      type,
      transactionTypeEnum.READ_WRITE,
      (store) => store.delete(entity.id),
    );
  }
}

export const indexedDbProvider = new IndexedDbProvider(STORAGE_KEY);
