import './styles/styles.sass';

import { createApp } from 'vue';

import App from './App.vue';
import { useFontAwesome } from './plugins/useFontAwesone';
import { router } from './router';

const app = createApp(App);

useFontAwesome(app);

app.use(router).mount('#app');
