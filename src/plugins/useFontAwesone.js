import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faEdit,
  faEye,
  faPlus,
  faTimes,
  faTrash,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

export function useFontAwesome(app) {
  library.add(faTrash);
  library.add(faEdit);
  library.add(faEye);
  library.add(faPlus);
  library.add(faTimes);

  app.component('FontAwesomeIcon', FontAwesomeIcon);
}
