export const RouteName = {
  TRANSITION_ADD: 'translation:add',
  TRANSITION_EDIT: 'translation:edit',
  TRANSITION_LIST: 'translation:list',
  TRANSITION_TRANSLATE: 'translation:translate',
  DICTIONARY: 'dictionary',
};

export const routes = [
  {
    path: '/translations',
    name: RouteName.TRANSITION_LIST,
    component: () => import('@/pages/Translations/TranslationsList.vue'),
  },
  {
    path: '/translations/add',
    name: RouteName.TRANSITION_ADD,
    component: () => import('@/pages/Translations/TranslationsAdd.vue'),
  },
  {
    path: '/translations/:id/edit',
    name: RouteName.TRANSITION_EDIT,
    component: () => import('@/pages/Translations/TranslationsEdit.vue'),
    props: (route) => ({ id: Number(route.params.id) }),
  },
  {
    path: '/translations/:id/translate',
    name: RouteName.TRANSITION_TRANSLATE,
    component: () => import('@/pages/Translations/TranslationsTranslate.vue'),
    props: (route) => ({ id: Number(route.params.id) }),
  },
  {
    path: '/dictionary',
    name: RouteName.DICTIONARY,
    component: () => import('@/pages/Dictionary/Dictionary.vue'),
  },
  {
    path: '/dev',
    name: 'dev',
    component: () => import('@/pages/Development.vue'),
  },
  {
    name: 'not-found',
    path: '/:pathMatch(.*)',
    redirect: { name: RouteName.TRANSITION_LIST },
  },
];
